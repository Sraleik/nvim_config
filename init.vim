set nocompatible
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.config/nvim/bundle/Vundle.vim
call vundle#begin('~/.config/nvim/bundle')
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/.config/nvim/bundle')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
" for opening file
Plugin 'kien/ctrlp.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'tpope/vim-commentary'
Plugin 'jayflo/vim-skip'
Plugin 'ervandew/supertab'
Plugin 'airblade/vim-gitgutter'
Plugin 'sickill/vim-monokai'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

let g:mapleader = "\<Space>"

" fish config{
" from https://github.com/tpope/vim-sensible/blob/master/plugin/sensible.vim
if &shell =~# 'fish$'
  set shell=/bin/bash
endif
" }

syntax enable

" line number
set number

" Make Y behave like C and D.
" taken from https://github.com/tpope/vim-sensible
nnoremap Y y$

set incsearch "is: automatically begins searching as you type
set hlsearch "hls: highlights search results; ctrl-n or :noh to unhighlight
nnoremap <Leader><Leader> :w<CR>
" Status bar
set laststatus=2

set showcmd
map <C-k><C-b> :NERDTreeToggle<CR>
set ignorecase  
set autoread 
set tabstop=2 shiftwidth=2 expandtab 
colorscheme monokai
hi Normal ctermbg=none
highlight NonText ctermbg=none
